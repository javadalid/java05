package deloitte.academy.lesson01.entity;

import java.util.ArrayList;

/**
 * Clase para crear una solicitud de productos a un proveedor.
 * 
 * @author Javier Adalid
 *
 */
public class Solicitud {

	private int idProveedor;
	private String nombreProducto;
	private int cantidad;
	private String fecha;

	public static ArrayList<Solicitud> solicitudes = new ArrayList<Solicitud>();

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getFecha() {
		return fecha;
	}

	/**
	 * Constructor de una solicitud.
	 * 
	 * @param idProveedor
	 * @param nombreProducto
	 * @param cantidad
	 * @param fecha
	 */
	public Solicitud(int idProveedor, String nombreProducto, int cantidad, String fecha) {
		super();
		this.idProveedor = idProveedor;
		this.nombreProducto = nombreProducto;
		this.cantidad = cantidad;
		this.fecha = fecha;
		solicitudes.add(this);
	}

	public int getIdProveedor() {
		return idProveedor;
	}

	public void setIdProveedor(int idProveedor) {
		this.idProveedor = idProveedor;
	}

	public String getNombreProducto() {
		return nombreProducto;
	}

	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

}
