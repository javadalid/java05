package deloitte.academy.lesson01.entity;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase que permite crear un nuevo almac�n.
 * 
 * @author Javier Adalid
 *
 */
public class Almacen {

	private int idAlmacen;
	public static final ArrayList<Almacen> almacenes = new ArrayList<Almacen>();
	private static final Logger LOGGER = Logger.getLogger(Almacen.class.getName());

	/**
	 * Constructor del almac�n, con la validaci�n incluida de que no exista otro con
	 * un ID igual.
	 * 
	 * @param idAlmacen
	 */
	public Almacen(int idAlmacen) {
		super();
		this.idAlmacen = idAlmacen;
		boolean existe = false;
		for (Almacen p : almacenes) {
			if (p.getIdAlmacen() == idAlmacen) {
				existe = true;
				break;
			}
		}
		if (!existe)
			almacenes.add(this);
		else
			LOGGER.log(Level.SEVERE, "Ya existe un almac�n con ese nombre. \n");
	}

	public int getIdAlmacen() {
		return idAlmacen;
	}

	public void setIdAlmacen(int idAlmacen) {
		this.idAlmacen = idAlmacen;
	}

}
