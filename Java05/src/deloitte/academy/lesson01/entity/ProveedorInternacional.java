package deloitte.academy.lesson01.entity;

import java.util.logging.Level;

import deloitte.academy.lesson01.abstractas.Proveedor;

/**
 * Clase de Proveedor Internacional. Hereda de Proveedor.
 * 
 * @author Javier Adalid
 *
 */
public class ProveedorInternacional extends Proveedor {

	private String pais;

	/**
	 * Constructor. Requiere de la especificaci�n del pa�s.
	 * 
	 * @param idProveedor, type int.
	 * @param pais,        type string.
	 */
	public ProveedorInternacional(int idProveedor, String pais) {
		super(idProveedor);
		this.pais = pais;
	}

	/**
	 * Sobreescritura del m�todo de respuesta a la solicitud.
	 */
	@Override
	public void respuestaSolicitud() {
		LOGGER.log(Level.INFO, "We have received your request! International Shipping. \n");
	}

}