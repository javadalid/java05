package deloitte.academy.lesson01.entity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

import deloitte.academy.lesson01.abstractas.Proveedor;
import deloitte.academy.lesson01.interfaz.Operaciones;

/**
 * Clase Tienda, que implementa la interfaz Operaciones.
 * 
 * @author Javier Adalid
 *
 */
public class Tienda implements Operaciones {

	private static final DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	private ArrayList<Producto> devueltos = new ArrayList<Producto>();
	private static final Logger LOGGER = Logger.getLogger(Tienda.class.getName());
	private int idTienda;

	public Tienda(int idTienda) {
		super();
		this.idTienda = idTienda;
	}

	public int getIdTienda() {
		return idTienda;
	}

	public void setIdTienda(int idTienda) {
		this.idTienda = idTienda;
	}

	/**
	 * M�todo para vender un producto. Requiere de un nombre de producto type string
	 * y la cantidad a vender type int.
	 */
	@Override
	public void vender(String nombre, int cantidad) {
		boolean found = false;
		boolean suficiente = false;
		for (Producto p : Producto.listaProductos) {
			if (nombre.toLowerCase().equals(p.getNombre().toLowerCase())) {
				found = true;
				if (p.getStock() >= cantidad) {
					suficiente = true;
				}
			}
			if (found && suficiente) {
				p.setStock(p.getStock() - cantidad);
				System.out.println("Total a pagar: $" + p.getPrecio() * cantidad + "\n");
				LOGGER.log(Level.INFO, "Venta realizada exitosamente! \n");
				break;
			}
		}
		if (!found)
			LOGGER.log(Level.SEVERE, "�El producto '" + nombre + "' no fue encontrado! \n");
		else if (!suficiente)
			LOGGER.log(Level.SEVERE, "�No hay suficiente Stock del producto '" + nombre + "' ! \n");

	}

	/**
	 * M�todo para remover un producto existente. Requiere un nombre type string
	 * como par�metro.
	 */
	@Override
	public void removerProducto(String nombre) {
		boolean found = false;
		for (Producto p : Producto.listaProductos) {
			if (nombre.toLowerCase().equals(p.getNombre().toLowerCase())) {
				found = true;
			}
			if (found) {
				Producto.listaProductos.remove(p);
				LOGGER.log(Level.INFO, "�Producto eliminado! \n");
				break;
			}
		}
		if (!found)
			LOGGER.log(Level.SEVERE, "�El producto '" + nombre + "' no fue encontrado! \n");

	}

	/**
	 * M�todo para devolver un producto a la tienda. Requiere de el nombre del
	 * producto, type string y la cantidad a devolver type int, como par�metros.
	 */
	@Override
	public void devolverProducto(String nombre, int cantidad) {
		boolean found = false;
		for (Producto p : Producto.listaProductos) {
			if (nombre.toLowerCase().equals(p.getNombre().toLowerCase())) {
				found = true;
			}
			if (found) {
				p.setStock(p.getStock() + cantidad);
				Calendar cal = Calendar.getInstance();
				p.setFechaDevolucion(sdf.format(cal.getTime()));
				LOGGER.log(Level.INFO, "�Producto devuelto y stock actualizado! \n");
				p.setDevuelto(p.getDevuelto() + cantidad);
				devueltos.add(p);
				break;
			}
		}
		if (!found)
			LOGGER.log(Level.SEVERE, "�El producto '" + nombre + "' no fue encontrado! \n");

	}

	/**
	 * M�todo que imprime el historial de solicitudes de nuevos productos.
	 */
	@Override
	public void historialSolicitud() {
		try {
			for (Solicitud s : Solicitud.solicitudes) {
				System.out.println("ID Proveedor: " + s.getIdProveedor() + " --- Producto: " + s.getNombreProducto()
						+ " --- Cantidad:  " + s.getCantidad() + " --- Fecha: " + s.getFecha());
			}
			System.out.println("---------------------------------------------------");
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "�Excepci�n producida!", ex + "\n");
		}

	}

	/**
	 * M�todo que imprime el historial de devoluciones de productos.
	 */
	@Override
	public void historialDevolucion() {
		try {
			for (Producto p : devueltos) {
				System.out
						.println(p.getNombre() + " --- " + p.getDevuelto() + " unidades --- " + p.getFechaDevolucion());
			}
			System.out.println("---------------------------------------------------");
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "�Excepci�n producida!", ex + "\n");
		}

	}

	/**
	 * M�todo para solicitar nuevos productos a proveedores. Requiere el id del
	 * proveedor, type int, el nombre del producto, type string y la cantidad type
	 * int, como par�metros.
	 */
	@Override
	public void solicitarNuevosProductos(int idProveedor, String nombreProd, int cantidad) {
		boolean found = false;
		Proveedor proveedor = null;
		for (Proveedor p : Proveedor.proveedores) {
			if (idProveedor == p.getIdProveedor()) {
				found = true;
				proveedor = p;
				break;
			}
		}
		if (!found)
			LOGGER.log(Level.SEVERE, "�No se encontr� ning�n proveedor con ese ID! \n");
		else {
			Calendar cal = Calendar.getInstance();
			String fecha = (sdf.format(cal.getTime()));
			new Solicitud(idProveedor, nombreProd, cantidad, fecha);
			LOGGER.log(Level.INFO, "�Solicitud realizada correctamente! \n");
			proveedor.respuestaSolicitud();
		}

	}

	/**
	 * M�todo que realiza el traspaso de un producto de un almac�n a otro. Requiere
	 * el nombre del producto, type string y el Id del almac�n de destino, type int.
	 */
	@Override
	public void traspasoAlmacen(String nombreProducto, int idAlmacenDestino) {
		boolean found = false;
		boolean found2 = false;
		Almacen nuevoAlmacen = null;
		try {
			for (Almacen a : Almacen.almacenes) {
				if (a.getIdAlmacen() == idAlmacenDestino) {
					found = true;
					nuevoAlmacen = a;
					break;
				}
			}
			if (!found)
				LOGGER.log(Level.SEVERE, "�No se encontr� ning�n almac�n con ese ID! \n");
			else {
				for (Producto p : Producto.listaProductos) {
					if (p.getNombre().toLowerCase().contentEquals(nombreProducto.toLowerCase())) {
						found2 = true;
					}
					if (found2 && nuevoAlmacen != null) {
						p.setAlmacen(nuevoAlmacen);
						LOGGER.log(Level.SEVERE, "�Traspaso de almacen correcto! \n");
					}
				}
				if (!found2)
					LOGGER.log(Level.SEVERE, "�No se encontr� ning�n producto con ese nombre! \n");
			}
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "�Excepci�n producida!", ex + "\n");

		}
	}

	/**
	 * M�todo para ver los productos que est�n dentro de un almac�n. Requiere de un
	 * almac�n, type Almacen.
	 */
	@Override
	public void verProductos(Almacen almacen) {
		for (Producto p : Producto.listaProductos) {
			if (p.getAlmacen().equals(almacen))
				System.out.println(p.getNombre() + " --- Stock: " + p.getStock() + " --- Precio: $" + p.getPrecio()
						+ " --- Categoria: " + p.getCategoria());
		}

	}
}
