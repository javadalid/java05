package deloitte.academy.lesson01.entity;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import deloitte.academy.lesson01.abstractas.Proveedor;
/**
 * Clase Producto.
 * @author Javier Adalid
 *
 */
public class Producto {

	public static final ArrayList<Producto> listaProductos = new ArrayList<Producto>();
	private static final Logger LOGGER = Logger.getLogger(Producto.class.getName());
	private String nombre;
	private double precio;
	private int stock;
	private Proveedor proveedor;
	private Categorias categoria;
	private Almacen almacen;
	private int devuelto;
	private String fechaDevolucion;

	/**
	 * Constructor de un producto con validación incluida para evitar productos
	 * repetidos.
	 * 
	 * @param nombre
	 * @param precio
	 * @param stock
	 * @param proveedor
	 * @param categoria
	 * @param almacen
	 */
	public Producto(String nombre, double precio, int stock, Proveedor proveedor, Categorias categoria,
			Almacen almacen) {
		super();
		boolean existe = false;

		this.nombre = nombre;
		this.precio = precio;
		this.stock = stock;
		this.proveedor = proveedor;
		this.categoria = categoria;
		this.almacen = almacen;
		this.devuelto = 0;
		this.fechaDevolucion = "";
		for (Producto p : listaProductos) {
			if (p.getNombre().toLowerCase().equals(nombre.toLowerCase())) {
				existe = true;
				break;
			}
		}
		if (!existe)
			listaProductos.add(this);
		else
			LOGGER.log(Level.SEVERE, "Ya existe un producto con ese nombre. \n");

	}

	public String getFechaDevolucion() {
		return fechaDevolucion;
	}

	public void setFechaDevolucion(String fechaDevolucion) {
		this.fechaDevolucion = fechaDevolucion;
	}

	public int getDevuelto() {
		return devuelto;
	}

	public void setDevuelto(int devuelto) {
		this.devuelto = devuelto;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public Proveedor getProveedor() {
		return proveedor;
	}

	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}

	public Categorias getCategoria() {
		return categoria;
	}

	public void setCategoria(Categorias categoria) {
		this.categoria = categoria;
	}

	public Almacen getAlmacen() {
		return almacen;
	}

	public void setAlmacen(Almacen almacen) {
		this.almacen = almacen;
	}

}
