package deloitte.academy.lesson01.entity;

/**
 * Enum de categorías disponibles en la tienda, para clasificación de productos.
 * 
 * @author Javier Adalid
 *
 */
public enum Categorias {
	LAPTOPS, ACCESORIOS, PANTALLAS, REPRODUCTORES;
}
