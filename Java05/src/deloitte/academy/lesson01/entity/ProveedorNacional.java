package deloitte.academy.lesson01.entity;

import java.util.logging.Level;

import deloitte.academy.lesson01.abstractas.Proveedor;

/**
 * Clase de Proovedor Nacional. Hereda de Proveedor.
 * 
 * @author Javier Adalid
 *
 */
public class ProveedorNacional extends Proveedor {

	public ProveedorNacional(int idProveedor) {
		super(idProveedor);
	}

	/**
	 * Sobreescritura del m�todo de respuesta a la solicitud.
	 */
	@Override
	public void respuestaSolicitud() {
		LOGGER.log(Level.INFO, "�Hemos recibido la solicitud! Env�o Nacional. \n");
	}

}
