package deloitte.academy.lesson01.interfaz;

import deloitte.academy.lesson01.entity.Almacen;

/**
 * Interface con todas las operaciones/acciones que lleva a cabo la tienda.
 * 
 * @author Javier Adalid
 *
 */
public interface Operaciones {

	public void vender(String nombre, int cantidad);

	public void removerProducto(String nombre);

	public void devolverProducto(String nombre, int cantidad);

	public void historialSolicitud();

	public void historialDevolucion();

	public void solicitarNuevosProductos(int idProveedor, String nombreProd, int cantidad);

	public void traspasoAlmacen(String nombreProducto, int idAlmacenDestino);

	public void verProductos(Almacen almacen);
}
