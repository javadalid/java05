package deloitte.academy.lesson01.run;

import deloitte.academy.lesson01.entity.Almacen;
import deloitte.academy.lesson01.entity.Categorias;
import deloitte.academy.lesson01.entity.Producto;
import deloitte.academy.lesson01.entity.ProveedorInternacional;
import deloitte.academy.lesson01.entity.ProveedorNacional;
import deloitte.academy.lesson01.entity.Tienda;

/**
 * Clase principal, main, en donde se implementan todos los m�todos de la
 * tienda.
 * 
 * @author Javier Adalid
 *
 */
public class EjecutarTienda {

	public static void main(String[] args) {
		// Creaci�n de tienda
		Tienda tienda = new Tienda(1);
		// Creaci�n de almacenes
		Almacen almacen1 = new Almacen(1);
		Almacen almacen2 = new Almacen(2);
		// Tira error por almacen duplicado
		Almacen almacen3 = new Almacen(2);
		// Creaci�n de proveedores
		ProveedorNacional compuMexico = new ProveedorNacional(1);
		ProveedorInternacional usaComputers = new ProveedorInternacional(2, "Estados Unidos");
		// Creaci�n de productos de prueba
		new Producto("laptop", 10500.50, 3, usaComputers, Categorias.LAPTOPS, almacen1);
		new Producto("audifonos", 300.99, 10, compuMexico, Categorias.ACCESORIOS, almacen1);
		new Producto("smarttv", 4000, 11, usaComputers, Categorias.PANTALLAS, almacen2);
		new Producto("reproductor", 1900.99, 1, compuMexico, Categorias.REPRODUCTORES, almacen2);
		// Venta exitosa
		tienda.vender("laptop", 2);
		// Venta fallida
		tienda.vender("laptop", 2);
		// Devolver producto
		tienda.devolverProducto("audifonos", 3);
		tienda.devolverProducto("smarttv", 10);
		// Devolver producto fallido
		tienda.devolverProducto("silla", 10);
		// Historial de devoluciones
		tienda.historialDevolucion();
		// Solicitud de producto
		tienda.solicitarNuevosProductos(1, "Impresora 3D", 100);
		tienda.solicitarNuevosProductos(2, "Silla de escritorio", 50);
		// Solicitud de producto fallida
		tienda.solicitarNuevosProductos(3, "Impresora 3D", 100);
		// Historial de solicitudes
		tienda.historialSolicitud();
		// Remover producto fallido
		tienda.removerProducto("reproductores");
		// Remover producto exitoso
		tienda.removerProducto("reproductor");
		// Ver los productos del almacen 1
		tienda.verProductos(almacen1);

	}

}
