package deloitte.academy.lesson01.abstractas;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase abstracta para crear un proveedor.
 * 
 * @author Javier Adalid
 *
 */
public abstract class Proveedor {
	protected static final Logger LOGGER = Logger.getLogger(Proveedor.class.getName());
	public static final ArrayList<Proveedor> proveedores = new ArrayList<Proveedor>();

	private int idProveedor;

	/**
	 * Constructor con validación de que no exista un proveedor con el mismo ID.
	 * 
	 * @param idProveedor
	 */
	public Proveedor(int idProveedor) {
		super();
		boolean existe = false;
		this.idProveedor = idProveedor;
		for (Proveedor p : proveedores) {
			if (p.getIdProveedor() == idProveedor) {
				existe = true;
				break;
			}
		}
		if (!existe)
			proveedores.add(this);
		else
			LOGGER.log(Level.SEVERE, "Ya existe un proveedor con ese ID. \n");
	}

	public int getIdProveedor() {
		return idProveedor;
	}

	public void setIdProveedor(int idProveedor) {
		this.idProveedor = idProveedor;
	}

	public abstract void respuestaSolicitud();
}
